def do_turn(pw):
    m = 0
    o = 0

    for planet in pw.my_planets():
        m += planet.num_ships()
    for planet in pw.enemy_planets():
        o += planet.num_ships()

    if m < o:
        for planet in pw.my_planets():
            for other in pw.neutral_planets():
                if pw.distance(planet, other) < 8:
                    pw.issue_order(planet, other, planet.num_ships())
                    break
    else:
        for planet, enemy in zip(pw.my_planets(), pw.enemy_planets()):
            pw.issue_order(planet, enemy, planet.num_ships())
    return
